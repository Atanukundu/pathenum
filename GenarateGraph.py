import matplotlib.pyplot as plt
import networkx as nx
import z3

G = nx.DiGraph()
file_name = "GraphInput.txt"
try:
	file = open(file_name)
	for line in file.readlines():
		data = line.split()
		x = data[0]
		y = data[1]
		G.add_edge(x, y)
		print(x,y)

except FileNotFoundError:
	print("Could not find the named file. Please check the file name and path, and re-run the program.")
	exit()

