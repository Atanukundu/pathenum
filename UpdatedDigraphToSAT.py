# Please imput the graph data as a text file containing the graph, in the form of edge list, that is, every line mentions the nodes joined by an edge, and in ascending order.
#For example,
'''
1 3
2 3
2 4
3 1
3 2
4 2
'''
#Note that '1 3' and '3 1' must be separately mentioned in case the graph is undirected graph.

# Modules and packages imported

import matplotlib.pyplot as plt
import networkx as nx
import z3

# Reading the graph from the file and storing as data structure

G = nx.DiGraph()

#print("Please enter the name of the file in which graph data is stored.")
#print("Note that both file names and file extensions are usually case sensitive.")
#name = input()

#file_name = name if ".txt" in name else name + ".txt"
#file_name = "GraphInput.txt"
file_name = "GraphInput.txt"
try:
	file = open(file_name)

	for line in file.readlines():
		data = line.split()
		x = data[0]
		y = data[1]
		G.add_edge(x, y)

	file.close()

except FileNotFoundError:
	print("Could not find the named file. Please check the file name and path, and re-run the program.")
	exit()


# Plotting the graph for ease of reference

nx.draw(G, with_labels=True, font_weight='bold', node_color='blue')
plt.show()

# Conversion of graph to SAT problem

while True:
	u = input("Enter source node.")
	if u not in nx.nodes(G):
		print("Node does not exist. Please try again.")
	else:
		break

while True:
	v = input("Enter destination node.")
	if v not in nx.nodes(G):
		print("Node does not exist. Please try again.")
	elif v == u:
		ch = input("Are you sure source = destination? (y/n) ").upper()
		if ch == "Y":
			break
		else:
			print("Please try again.")
	else:
		break

t = int(input("Enter bound."))
k = 1;
count1 = 0

while (k <= t):

	s = z3.Solver()

	# INIT Step
	exp1 = z3.Bool("exp1")
	exp1 = True
	for i in nx.nodes(G):
		if i == u:
			exp1 = z3.And(exp1, z3.Bool(f"v{i}0"))
		else:
			exp1 = z3.And(exp1, z3.Not(z3.Bool(f"v{i}0")))

	# NEXT Step
	exp2 = z3.Bool("exp2")
	exp2 = True
	for i in range(k):
		for j in nx.nodes(G):
			exp2a = z3.Bool("exp2a")
			exp2a = False
			for l in nx.neighbors(G, j):
				exp2a = z3.Or(exp2a, z3.Bool(f"v{l}{i+1}"))            
			exp2 = z3.And(exp2, z3.Implies(z3.Bool(f"v{j}{i}"), exp2a))

	#EXCLUDE Step
	exp3 = z3.Bool("exp3")
	exp3 = True
	for i in range(k):
		for j in nx.nodes(G):
			exp3a = z3.Bool("exp3a")
			exp3a = True
			for l in nx.nodes(G):
				if l != j:
					exp3a = z3.And(exp3a, z3.Not(z3.Bool(f"v{l}{i+1}")))
			exp3 = z3.And(exp3, z3.Implies(z3.Bool(f"v{j}{i+1}"), exp3a))

	#TARGET Step
	exp4 = z3.Bool(f"v{v}{k}")

	# Writing all possible solutions to a new file

	file_name = "newfile.txt"
	if k == 1:
		file0 = open(file_name, "w")
	else:
		file0 = open(file_name, "a")
	s.add(exp1, exp2, exp3, exp4)
	file0.write("\n\nSolutions for the path problem with vertex length ")
	file0.write(str(k))
	file0.write(": \n\n")
	count = 1
	print(s)
	while True:
		if s.check() == z3.unsat:
			if count == 1:
				file0.write("unsat")
			break
		file0.write("\n[")
		for i in nx.nodes(G):
			for j in range(k+1):
				x = z3.Bool(f"v{i}{j}")
				if (str(s.model()[x]) == "True"):
					file0.write(f"v{i}{j} = " + str(s.model()[x]))
					file0.write("\n")
		file0.write("]\n")
		exp = z3.Bool("exp")
		exp = False
		for i in nx.nodes(G):
			for j in range(k+1):
				x = z3.Bool(f"v{i}{j}")
				exp = z3.Or(exp, x != s.model()[x])
		s.add(exp)
		count += 1

	file0.close()
	k += 1
	count1 += count-1
	print(count)
print("Paths found:",count1)
